" Bens local vim config file (modified from Henry's henry@henryjenkins.name)
" ben.norquay@gmail.com

filetype plugin indent on
set autoread                    " Set to read when a file is changed from the outside
set backspace=indent,eol,start  " more powerful backspacing
set expandtab
set history=500                 " keep 500 lines of command line history
set hlsearch
set ic                          " Case insensitive search
set mouse=a
set nocompatible
set nomodeline
set nowrap
set number
set ruler                       " show the cursor position all the time
set scrolloff=5
set shiftwidth=4
set showmatch                   " Show matching brackets.
set smartindent
set spell
set spelllang=en_nz
set suffixes+=.bak,~,.swp,.o,.info,.aux,.log,.dvi,.bbl,.blg,.brf,.cb,.ind,.idx,.ilg,.inx,.out,.toc
set tabstop=4
set viminfo='20,\"50
syntax enable


if has("autocmd")
    autocmd BufReadPost *  if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
    autocmd BufRead *.cpp,*.c,*.h setlocal tw=80
    autocmd BufEnter *.mib syntax sync fromstart | setlocal nospell
    autocmd FileType html  setlocal tw=175 tabstop=2 shiftwidth=2 omnifunc=htmlcomplete#CompleteTags
    autocmd FileType php   setlocal tabstop=4 shiftwidth=4 omnifunc=htmlcomplete#CompleteTags
    autocmd FileType css   setlocal tabstop=2 shiftwidth=2
    autocmd FileType javascript setlocal tabstop=2 shiftwidth=2
    autocmd FileType make  setlocal noexpandtab
    autocmd FileType tex\|plaintex   setlocal tw=80 tabstop=2 shiftwidth=2
    autocmd FileType vhdl  setlocal tabstop=2 shiftwidth=2
endif

" Highlighting and theme
set background=dark
hi rightMargin term=bold ctermfg=blue guifg=blue
hi clear SpellBad
hi SpellBad cterm=underline

map <F2> :setlocal spell!<cr>
map <F3> :call CompileRunjava()<CR>
map <F4> <esc>:n .<cr>
map <F5> :call CompileRunGcc()<CR>
map <F6> :call MakePdfLatex()<CR>
map <F7> <esc>:bn<cr>
map <F8> <esc>:bp<cr>

cmap w!! w !sudo tee > /dev/null %

func! CompileRunjava()
    exec "up"
    exec "javac ./*.java"
endfunc
func! CompileRunGcc()
    exec "up"
    exec "make"
endfunc
func! MakePdfLatex()
    exec "up"
    exec "!pdflatex report.tex"
endfunc
