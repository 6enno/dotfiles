# README #

Here is my config files for zsh, ssh, git and vim. I think that these are a fairly simple set of configs and they are all in one place and easy to manage.

Feel free to fork or create a branch or just use these configs, I am also interested in any suggestions that you peeps might have.

### How do I get set up? ###

* Clone the repo
* run ./install.sh

Note that any files that are listed in the install script will be copied into ./backup and replaced with a symlink. This way its easy to track the config changes that we make over time.

### What else do I need ###

Nothing really but I really recommend using all the tools that this is used to configure:

* Vi Improved
* Z shell
* Git
* Secure Shell

### Did I really put my ssh files online ###

Yep. This was not initially supposed to be a public repo but hey things change, so I have taken them out and changed all of the ones that I shared. All they keys are useless to anyone outside my local network anyway. The config is still included as that contains useful examples for peeps that want to have their own ssh configs. Also check out the sweet connection sharing trick.

### Who do I talk to? ###

Talk to me if you want, I can be friendly or a dick depending on my mood. ben.norquay@gmail.com