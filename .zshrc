# Variables
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
export EDITOR=vi

# Display time data for any command that takes
# longer than 1 min to run
export REPORTTIME=60

#PATH changes
if [ -d "$HOME/bin" ] ; then
    export PATH=/usr/local/kde4/bin:$PATH
fi
if [ -d "$HOME/Scripts" ] ; then
    PATH="$HOME/Scripts:$PATH"
fi
if [ -d "$HOME/testScripts" ] ; then
    PATH="$HOME/testScripts:$PATH"
fi
if [ -d "$HOME/google_appengine" ] ; then
    PATH="$HOME/google_appengine:$PATH"
fi

# Options
setopt appendhistory autocd extendedglob nomatch
unsetopt beep

# Prompt
autoload -Uz compinit promptinit
compinit
promptinit
prompt walters
source ~/.zsh/zshrc.sh
PROMPT='%{$fg_no_bold[white]%}→ %{$reset_color%}'
RPROMPT='%{$fg_no_bold[green]%} %{$reset_color%}'
RPROMPT='%{$fg_no_bold[green]%}%~%b$(git_super_status)%{$reset_color%}'

# Colour standard error output (its actually magenta)
#sm_color_red="$(  tput setaf 5)"
#sm_color_reset="$(tput sgr0   )"
#exec 2>>( awk '{print "'"$sm_color_red"'"$0"'"$sm_color_reset"'"}' >&2 & )

# Aliaii
alias cd..="cd .."
alias df="df -h"
alias du="du -h"
alias gk="gitk --all"
alias grep="grep --color"
alias gti="git"
alias la="ls -al"
alias ll="ls -l"
alias ls="ls --color -h"
alias maek="make"
alias mkae="make"
alias mv="mv -n"
alias rm="rm -I"
alias minicom="minicom -con"
alias csrdate='echo date -s \"`date -u --rfc-3339=sec`\" \&\& hwclock --systohc'

# Bindings
bindkey -e
bindkey "\e[1~" beginning-of-line # Home
bindkey "\e[4~" end-of-line # End
bindkey "\e[Z" reverse-menu-complete # Shift+Tab
bindkey "^[[A" history-search-backward # Up
bindkey "^[[B" history-search-forward # Down

# Completions
zstyle ':completion:*' matcher-list 'r:|[._-]=* r:|=* m:{a-zA-Z}={A-Za-z}' 'l:|=* r:|=*'
zstyle ':completion:*' list-colors ''
zstyle ':completion:*:*:*:*:*' menu select
zstyle ':completion:*:*:kill:*:processes' list-colors '=(#b) #([0-9]#) ([0-9a-z-]#)*=01'
zstyle ':completion:*:*:*:*:processes' command "ps -u `whoami` -o pid,user,comm -w -w"

# Disable named-directories autocompletion
zstyle ':completion:*:cd:*' tag-order local-directories directory-stack path-directories
cdpath=(. ~ ~/sources)

# Use /etc/hosts and known_hosts for hostname completion
if [[ -r ~/.ssh/known_hosts ]] _ssh_hosts=(${${${${(f)"$(<$HOME/.ssh/known_hosts)"}:#[\|]*}%%\ *}%%,*})
if [[ -r /etc/hosts ]] : ${(A)_etc_hosts:=${(s: :)${(ps:\t:)${${(f)~~"$(</etc/hosts)"}%%\#*}##[:blank:]#[^[:blank:]]#}}}
known_hosts=(
  "$_ssh_hosts[@]"
  "$_etc_hosts[@]"
  $(hostname)
  localhost
)
zstyle ':completion:*:hosts' hosts $known_hosts

# Use caching so that commands like apt and dpkg complete are useable
zstyle ':completion::complete:*' use-cache 1
zstyle ':completion::complete:*' cache-path /tmp/zsh-completion/cache/

autoload -U compinit
compinit

