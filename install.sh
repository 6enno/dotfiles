#!/bin/bash

tracking=(
~/.ssh/config
~/.zsh
~/.gitconfig
~/.vimrc
~/.zshrc
)

mkdir -p ./backup

for file in ${tracking[*]}
do
    if test -h $file
    then
        echo Will not backup $file as it is a symlink
    else
        echo Backing up $file
        cp -r $file ./backup

        # Remove the file and replace it with a link to this one
        rm -rf $file
        ln -s $PWD/`basename $file` $file
    fi
done
        
